create database role if not exists app_user;
create schema if not exists s1;
create function if not exists s1.get_ver()
    returns string
    as $$'v1.0'$$;
grant usage on schema s1 to database role app_user;
grant usage on function s1.get_ver() to database role app_user;
